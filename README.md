# BAudio

A simple CLI tool to batch convert flac files to mp3 files. Preserves folder
structure and audio tags.

## Install

There is currently no pypi package available, but you can install directly from
source. Simply clone the repository:

```
$ git clone https://gitlab.com/stbalduin/baudio.git
```

Then `cd` into the directory and install with pip:

```
$ cd baudio
$ pip install --user .
```

## Usage

* TODO: needs to be updated
