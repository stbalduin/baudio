# Patch Notes

## Version 0.3.0 (2021-06-07)

* Migrated to gitlab.com.
* New name of the tool baudio (Balduin Audio).
* Skip existing files unless `--force` flag is provided.
* Added changelog file.